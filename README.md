# Desafio - Desenvolvedor Full Stack Sênior

## Objetivo

Desenvolver uma aplicação web full stack para uma plataforma de streaming de vídeos avançada, que permita aos usuários assistir, favoritar, comentar, compartilhar vídeos e realizar transmissões ao vivo. A aplicação deve ser construída utilizando React ou Angular no frontend e Node.js ou PHP (com Laravel) no backend, com persistência de dados em um banco de dados.

## Requisitos

### Frontend (React ou Angular)

- **Interface de Usuário:**
  - Criar uma interface de usuário responsiva e intuitiva, utilizando React ou Angular, com uma experiência de streaming de vídeos envolvente.
  - Implementar funcionalidades para busca, visualização, reprodução, favoritação, comentário e compartilhamento de vídeos.
  - Implementar suporte para transmissões ao vivo e integração com serviços de live streaming (e.g., WebRTC, HLS).
  - Permitir aos usuários criar e gerenciar playlists de vídeos, incluindo a capacidade de adicionar, remover e reorganizar vídeos na playlist.
  - Desenvolver páginas separadas para exibir categorias de vídeos, permitindo aos usuários explorar conteúdo com base em seus interesses.
  - Integrar autenticação de usuários, permitindo o registro e login de contas de usuário, bem como a proteção de rotas privadas que requerem autenticação.
  - Utilizar chamadas de API para se comunicar com o backend e manipular dados de vídeos, usuários e playlists de forma assíncrona.

- **Desempenho e SEO:**
  - Implementar práticas de otimização de desempenho para garantir carregamento rápido e uma experiência de usuário suave.
  - Utilizar técnicas de SEO para garantir que a aplicação seja amigável para motores de busca.

### Backend (Node.js ou PHP + Laravel)

- **API RESTful:**
  - Criar uma API RESTful utilizando Node.js ou PHP e Laravel para lidar com as operações CRUD (Create, Read, Update, Delete) de vídeos, usuários e playlists.
  - Implementar endpoints para autenticação de usuários, registro, login e logout, utilizando tokens JWT (JSON Web Tokens) para autenticação stateless.
  - Desenvolver serviços para manipular as operações de banco de dados, incluindo a criação, leitura, atualização e exclusão de vídeos, usuários e playlists, utilizando um ORM (Object-Relational Mapping) como Sequelize (Node.js) ou Eloquent (Laravel) para interagir com o banco de dados.
  - Integrar middleware de autenticação para proteger as rotas privadas da API, garantindo que apenas usuários autenticados possam acessar recursos protegidos.
  - Implementar validação de entrada de dados nos endpoints da API, garantindo a integridade dos dados e prevenindo ataques de injeção.
  - Implementar suporte para transmissões ao vivo, integrando serviços como WebRTC ou outros protocolos de streaming ao vivo.

- **Escalabilidade e Manutenção:**
  - Propor uma arquitetura escalável para suportar um grande número de usuários e transmissões ao vivo simultâneas.
  - Implementar um sistema de log e monitoramento para acompanhar a performance da aplicação e identificar possíveis problemas.

### Banco de Dados

- Configurar um banco de dados não relacional para armazenar informações sobre vídeos, playlists e outros dados relevantes para a plataforma de streaming de vídeos.
- Configurar um banco de dados relacional para armazenar usuários e informações de assinaturas.
- Definir modelos de dados utilizando o Sequelize ORM (Node.js) ou Eloquent ORM (Laravel) para representar entidades de vídeo, usuário e playlist, e configurar associações entre eles conforme necessário.

### Segurança

- Implementar práticas de segurança robustas para proteger dados de usuários e garantir a integridade da aplicação.
- Garantir que as transmissões ao vivo estejam protegidas contra acessos não autorizados.

### Otimização de Desempenho

- Implementar uma estratégia básica de cache para reduzir a sobrecarga no servidor durante o streaming de vídeos.
- Comprimir os vídeos antes de armazená-los, se possível, para reduzir o tamanho do arquivo e melhorar a velocidade de transferência.

### Testes

- Implementar testes automatizados para os componentes do frontend (React ou Angular), incluindo testes de unidade para lógica de negócios e testes de integração para simular interações do usuário.
- Desenvolver testes de integração para os endpoints da API, cobrindo casos de uso de sucesso e cenários de erro.
- Implementar testes de carga e desempenho para garantir que a aplicação possa lidar com um grande número de usuários simultâneos.

## Diferenciais

- Publicação no Vercel.app
- Uso de Containers Docker
- Build para produção
- Implementação de arquitetura de microsserviços para componentes críticos da aplicação.

## Dicas

- Utilize conceitos avançados do framework escolhido (React ou Angular) para criar uma aplicação modular, escalável e fácil de manter, proporcionando uma experiência de streaming de vídeos imersiva para os usuários.
- Faça uso extensivo das funcionalidades oferecidas pelo framework escolhido, como hooks no React ou serviços no Angular, para gerenciar estado e efeitos colaterais de forma eficiente.
- Utilize técnicas de otimização de desempenho específicas do framework escolhido para garantir uma experiência de streaming de vídeos responsiva.
- Mantenha uma arquitetura limpa e bem organizada tanto no frontend quanto no backend, seguindo princípios de design como SOLID e separação de interesses.
- Desenvolva a solução de acordo com os requisitos especificados, garantindo uma plataforma de streaming de vídeos robusta, escalável e de alta qualidade.

## Como Contribuir

1. Faça um fork deste repositório.
2. Crie uma branch com a sua feature: `git checkout -b feature/nova-feature`
3. Faça commit das suas mudanças: `git commit -m 'Adiciona nova feature'`
4. Faça push para a sua branch: `git push origin feature/nova-feature`
5. Faça um pull request neste repositório.

# Boa Sorte!!
